import Influx.Applications.ApplicationsRunning
import Influx.Alarm.Deadman.Application (program)
import Control.Logging

main :: IO ()
main = program  applicationsRunning LevelInfo