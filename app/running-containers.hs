import Influx.Applications.RunningContainers
import Influx.Alarm.Deadman.Application (program)
import Control.Logging

main :: IO ()
main = program runningContainers LevelInfo
