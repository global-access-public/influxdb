{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Servant.Streaming.Client.SafeT where

import Control.Monad.IO.Class ( MonadIO (..))
import Control.Monad.Trans.Control ( liftBaseWith)
import Pipes.Safe ( SafeT , runSafeT)
import Servant.API.Stream
  ( FromSourceIO (..)
  , SourceIO
  , ToSourceIO (..)
  )
import Servant.Types.SourceT (StepT)
import qualified Servant.Types.SourceT as S
import Streaming (Of ((:>)))
import Streaming.Internal (Stream (..))

class StreamToSourceIO m where
  streamToSourceIO :: Stream (Of b) m () -> SourceIO b

instance StreamToSourceIO IO where
  streamToSourceIO ma = S.SourceT ($ go ma)
    where
      go :: Stream (Of b) IO () -> S.StepT IO b
      go (Return ()) = S.Stop
      go (Effect p) = S.Effect (fmap go p)
      go (Step (b :> n)) = S.Yield b (go n)

instance StreamToSourceIO (SafeT IO) where
  streamToSourceIO ma =
    S.SourceT $ \k ->
      runSafeT $
        liftBaseWith $ \runSafe -> k (consumeStream runSafe ma)


consumeStream :: Functor f => (f (StepT m a) -> m (StepT m a)) -> Stream (Of a) f () -> StepT m a
consumeStream _ (Return ()) = S.Stop
consumeStream runSafe (Effect p) = S.Effect $ runSafe $ fmap (consumeStream runSafe) p
consumeStream runSafe (Step (b :> n)) = S.Yield b (consumeStream runSafe n)



instance StreamToSourceIO m => ToSourceIO b (Stream (Of b) m ()) where
  toSourceIO = streamToSourceIO

instance (MonadIO m) => FromSourceIO b (Stream (Of b) m ()) where
  fromSourceIO src = pure $ Effect $ liftIO $ S.unSourceT src (return . go)
    where
      go :: S.StepT IO b -> Stream (Of b) m ()
      go S.Stop = Return ()
      go (S.Error err) = Effect (liftIO (fail err))
      go (S.Skip s) = go s -- drives
      go (S.Effect ms) = Effect (liftIO (fmap go ms))
      go (S.Yield x s) = Step (x :> go s)
