{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module UpdateConfig.Git where

import Control.Monad.Catch (MonadCatch, catchAll)
import Control.Monad.Cont (ContT (ContT, runContT))
import Data.Yaml (FromJSON, decodeFileThrow)
import Deriving.Aeson
  ( CustomJSON (CustomJSON)
  , FieldLabelModifier
  , StripPrefix
  )
import Influx.Lib ( Logger )
import Protolude
    ( ($),
      Functor((<$)),
      Show,
      Applicative(pure),
      Generic,
      Semigroup((<>)),
      Monoid(mempty),
      Bool(..),
      IO,
      Text,
      finally,
      SomeException,
      either,
      (.),
      (=<<),
      not,
      MonadIO,
      (&),
      FilePath )
import Turtle
  ( cd
  , cp
  , inshellWithErr
  , lineToText
  , mktree
  , pwd
  , sh
  , (</>)
  )

data DataSource = DataSource
  { dataSource_url :: Text
  , dataSource_dataPath :: FilePath
  , dataSource_templatePath :: FilePath
  , dataSource_optionsPath :: FilePath
  }
  deriving stock (Generic, Show)
  deriving
    (FromJSON)
    via CustomJSON '[FieldLabelModifier '[StripPrefix "dataSource_"]] DataSource

data UpdateConfigLog
  = UpdateConfigDone
  | UpdateConfigFailed SomeException
  | UpdateConfigTryFailed SomeException
  | UpdateStdErr Text
  | UpdateStdOut Text
  | UpdateConfigImpossible
  deriving (Show)

tryOr :: (MonadCatch m, MonadIO m) => Logger UpdateConfigLog -> m r -> ContT Bool m ()
tryOr logger action = do
  (logger . UpdateConfigTryFailed) =<< ContT (catchAll $ True <$ action)

tmp :: FilePath
tmp = "/tmp/repocache"

updateConfig
  :: Logger UpdateConfigLog -- ^ logger
  -> FilePath -- ^ git configuration
  -> FilePath -- ^ where do copy the csv data
  -> FilePath -- ^ where to copy the template
  -> FilePath -- ^ where to copy dynamic options file 
  -> IO ()
updateConfig logger gitlabConfig dataPath templatePath optionsPath = do
  catchAll
    do
      DataSource {..} <- decodeFileThrow gitlabConfig
      mktree tmp
      here <- pwd
      r <- finally
        do
          cd tmp
          pure & runContT do
            tryOr logger do
              command logger $ do
                "git clone " <> dataSource_url <> " ."
            tryOr logger do
              command logger "git config pull.rebase false"
              command logger "git pull "
            pure False
        do cd here
      if not r
        then logger UpdateConfigImpossible
        else do
          cp (tmp </> dataSource_dataPath) dataPath
          cp (tmp </> dataSource_templatePath) templatePath
          cp (tmp </> dataSource_optionsPath) optionsPath

    do logger . UpdateConfigFailed
  logger UpdateConfigDone

command :: MonadIO io => Logger UpdateConfigLog -> Text -> io ()
command logger func = sh do
  lr <- inshellWithErr func mempty
  logger $ either (UpdateStdErr . lineToText) (UpdateStdOut . lineToText) lr
