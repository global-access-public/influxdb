{-# LANGUAGE BlockArguments #-}
module Influx.Async where

import qualified Streaming.Prelude as S
import Streaming
import Protolude
import Influx.AccessStreaming
import Prelude hiding (print, FilePath)
import Influx.Read (queryS)
import Control.Monad.Extra (whenJustM)
import Control.Concurrent.STM (newTBQueueIO, writeTBQueue, TBQueue, readTBQueue)

withStreamingInflux
  :: FilePath -- ^ influx access data
  -> Text
  -> (Stream (Of ByteString) IO () -> IO r) -- ^
  -> IO r
withStreamingInflux fp query f = do
  WithInfluxS streamIn' _ <- withInfluxS fp
  streamIn'
    do queryS (const $ pure ()) query
    do f . getStreaming


streamFromChannel :: MonadIO m => TBQueue (Maybe a) -> Stream (Of a) m ()
streamFromChannel chan = whenJustM
  do liftIO $ atomically $ readTBQueue  chan
  do \x -> S.yield x >> streamFromChannel chan

asyncInfluxStream
  :: MonadIO m
  => Int -- ^ channel bound
  -> FilePath -- ^ influx
  -> Text -- ^ query
  -> IO (Stream (Of ByteString) m ())
asyncInfluxStream n fp query = do
  chan <- newTBQueueIO $ fromIntegral n
  let write = atomically . writeTBQueue  chan
  link <=< async $ do
    withStreamingInflux fp query $ \xs -> S.mapM_
      do write
      do S.map Just xs <> S.yield Nothing
  pure $ streamFromChannel chan

-- safeStream
--   :: ( MonadMask m
--      , MonadBaseControl IO m
--      , MonadBase IO n
--      )
--   => Stream (Of a) m ()
--   -> (Stream (Of a) n () -> m r)
--   -> m r
-- safeStream s = withBuffer (bounded 1000) (writeStreamBasket s) . flip withStreamBasket
