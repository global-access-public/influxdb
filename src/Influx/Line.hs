{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Influx.Line where

import Control.Lens (makeLensesFor)
import Data.Aeson (FromJSON, ToJSON)
import Data.Csv (FromField)
import qualified Data.Map as M
import Data.Serialize (Serialize (..))
import Data.Serialize.Text ()
import qualified Data.Text as T
import Data.Time.Clock.POSIX (POSIXTime)
import Influx.Data (Data (..))
import Protolude hiding (get, put)

data Tag = Tag {tag_key :: Text, tag_value :: Text}
  deriving (Show)
  deriving (Generic, FromJSON, ToJSON)

instance Serialize Tag

type Tags = Map Text Text

fromTags :: Tags -> [Tag]
fromTags x = uncurry Tag <$> M.assocs x

data Field = Field Text Data
  deriving (Show)
  deriving (Generic, FromJSON, ToJSON)

instance Serialize Field

newtype Measurement = Measurement {renderMeasurement :: Text}
  deriving newtype (IsString, Show, FromJSON, ToJSON, FromField, Eq, Hashable)
  deriving stock (Generic)

instance Serialize Measurement

rw :: Text -> Text
rw = T.replace " " "_"

renderTag :: Tag -> Text
renderTag (Tag t v) = rw t <> "=" <> rw v

renderField :: Field -> Text
renderField (Field t (StringData v)) = rw t <> "=" <> show v
renderField (Field t (IntData v)) = rw t <> "=" <> show v <> "i"
renderField (Field t (DoubleData v)) = rw t <> "=" <> show v
renderField (Field t (BoolData v)) = rw t <> "=" <> show v

renderS :: (a -> Text) -> [a] -> Text
renderS r = T.intercalate "," . fmap r

data Line = Line
  { measurement :: Measurement
  , tags :: [Tag]
  , fields :: [Field]
  , timestamp :: Maybe POSIXTime
  }
  deriving (Show, Generic)

makeLensesFor
  [ ("measurement", "measurementL")
  , ("tags", "tagsL")
  , ("fields", "fieldsL")
  , ("timestamp", "timestampL")
  ]
  ''Line

instance Serialize Line

instance Serialize POSIXTime where
  get = realToFrac @Double <$> get
  put = put @Double . realToFrac

data Precision = Second | MilliSecond
  deriving (Show, Generic, FromJSON, ToJSON) 

renderLine :: Precision -> Line -> Text
renderLine p Line {..} =
  let factor = case p   of 
        Second -> identity   
        MilliSecond -> (* 1000)
  in
  renderMeasurement measurement
    <> do
      case tags of
        [] -> " "
        _ -> "," <> renderS renderTag tags <> " "
    <> renderS renderField fields
    <> maybe "" (\s -> " " <> show @Int (factor $ floor s)) timestamp

sample :: Measurement -> [Tag] -> [Field] -> Line
sample a b c = Line a b c Nothing
