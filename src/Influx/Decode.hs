{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Influx.Decode where

import Control.Exception (throw)
import Control.Monad.Catch (MonadCatch)
import qualified Data.ByteString.Char8 as B
import Data.Csv
import qualified Data.Csv.Incremental as I
import Protolude
import Streaming
import qualified Streaming.Prelude as S

newtype ReadException = ReadException String
  deriving (Show, Generic, Exception)

-- | break the tables on empty lines
tables :: Monad m => Stream (Of ByteString) m r -> Stream (Stream (Of ByteString) m) m r
tables = S.breaks (== "\n")

-- | decode one table with named record parsing
decodeNamedS
  :: (MonadCatch m, FromNamedRecord a)
  => Stream (Of ByteString) m r -- ^ lines to decode
  -> Stream (Of a) m r
decodeNamedS = go I.decodeByName
  where
    go :: MonadCatch m => I.HeaderParser (I.Parser a) -> Stream (Of ByteString) m r -> Stream (Of a) m r
    go (I.DoneH _h p) s = consume p s
    go (I.PartialH feed) s = effect $ do
      r <- S.next s
      pure $ case r of
        Left q -> pure q
        Right (x, rest) -> go (feed x) rest
    go (I.FailH _ x) _ = throw $ ReadException x

-- | decode one table with positional record parsing
decodeS
  :: (MonadCatch m, FromRecord a)
  => Stream (Of ByteString) m r
  -> Stream (Of a) m r
decodeS = consume $ I.decode HasHeader

-- internal decoding, common to both decoders
consume
  :: MonadCatch m
  => I.Parser a
  -> Stream (Of ByteString) m r
  -> Stream (Of a) m r
consume (I.Many xs feed) s = do
  S.concat $ S.each xs
  effect $ do
    r <- S.next s
    pure $ case r of
      Left q -> pure q
      Right (x, rest) -> consume (feed x) rest
consume (I.Done xs) s = do
  S.concat $ S.each xs
  lift $ S.effects s
consume (I.Fail _ x) _ = throw $ ReadException x

-- | decode all tables with named record parsing
decodeAllNamedS
  :: forall a m r.
  (MonadCatch m, FromNamedRecord a)
  => Stream (Of ByteString) m r
  -> Stream (Of a) m r
decodeAllNamedS = concats . S.maps decodeNamedS . tables

-- | decode all tables with positional record parsing
decodeAllS
  :: forall a m r.
  (MonadCatch m, FromRecord a)
  => Stream (Of ByteString) m r
  -> Stream (Of a) m r
decodeAllS = concats . S.maps decodeS . tables

-- | just decode as Records
rawData :: forall m r. MonadCatch m =>  Stream (Of ByteString) m r -> Stream (Of Record) m r
rawData = decodeAllS

-- | just decode as (Map Text Text)
rawNamedData :: MonadCatch m => Stream (Of ByteString) m r -> Stream (Of (Map Text Text)) m r
rawNamedData = decodeAllNamedS

-- | do not decode, collect the stream in one output
blob :: Monad m => Stream (Of ByteString) m r -> m ByteString
blob s = B.unlines <$> S.toList_ s
