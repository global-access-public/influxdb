{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Influx.Lib where

import Control.Lens (makeLensesFor, (^.))
import Data.Aeson (FromJSON, ToJSON)
import Data.ByteString.Lens (unpackedChars, IsByteString (packedChars))
import Data.Csv (FromField (..), ToField (..))
import Data.Time
  ( UTCTime
  , defaultTimeLocale
  , parseTimeM
  )
import Data.Time.Clock.POSIX (POSIXTime)
import Data.Time.Format.ISO8601 (iso8601ParseM, iso8601Show)
import Protolude 
import qualified Data.Text as T
import Servant.API
  ( Required
  , ToHttpApiData (toUrlPiece)
  , QueryParam'
  )

type ReqParam = QueryParam' '[Required]

instance ToField UTCTime where
  toField x = iso8601Show  x  ^. packedChars 

instance FromField UTCTime where
  parseField x = iso8601ParseM (x ^. unpackedChars)
instance FromField POSIXTime where
  parseField x = parseTimeM True defaultTimeLocale "%s" (x ^. unpackedChars)


type Logger a = forall m. MonadIO m => a -> m ()

newtype Stripped = Stripped {strippedText :: Text}
  deriving newtype (Show, IsString, FromJSON, ToJSON, ToHttpApiData, Hashable, Eq)

instance FromField Stripped where
  parseField x = Stripped . T.strip <$> parseField x

-- | authorization token 
newtype Token = Token Text deriving newtype (Show, IsString, FromJSON, ToJSON)

-- | buckets
newtype Bucket = Bucket Text deriving newtype (Show, IsString, FromJSON, ToJSON, ToHttpApiData, Hashable, Eq )
newtype Host = Host Text deriving newtype (Show, IsString, FromJSON, ToJSON, ToHttpApiData)
newtype Container = Container Text deriving newtype (Show, IsString, FromJSON, ToJSON, ToHttpApiData)

deriving via Stripped instance FromField Host
deriving via Stripped instance FromField Bucket 
deriving via Stripped instance FromField Container 
-- | orgs
newtype Organization = Organization Text deriving newtype (Show, IsString, FromJSON, ToJSON, ToHttpApiData )
data OrgConfig w = OrgConfig
  { org_config :: Organization
  , org_config_application :: w
  }
  deriving (Generic, FromJSON, ToJSON, Show)
makeLensesFor
  [ ("org_config_application", "orgConfigApplication")
  ]
  ''OrgConfig

instance ToHttpApiData Token where
  toUrlPiece (Token t) = "Token " <> t

