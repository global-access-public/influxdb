{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE KindSignatures #-}

module Influx.Data where

import Control.Monad.Fail (MonadFail (fail))
import Data.Aeson (FromJSON, ToJSON)
import Data.Csv (FromField (..))
import Data.Serialize (Serialize)
import Data.Serialize.Text ()
import Data.Tagged (Tagged (Tagged, unTagged))
import Protolude

data Data
  = IntData Int64
  | DoubleData Double
  | StringData Text
  | BoolData Bool
  deriving (Show)
  deriving (Generic, FromJSON, ToJSON)

intData :: Integral a => a -> Data
intData = IntData . fromIntegral

instance Serialize Data

data E = forall v. FromField (Tagged v Data) => E (forall a. Tagged v a -> a)

instance FromField (Tagged Int Data) where
  parseField x = Tagged . IntData <$> parseField x

instance FromField (Tagged Double Data) where
  parseField x = Tagged . DoubleData <$> parseField x

instance FromField (Tagged Text Data) where
  parseField x = Tagged . StringData <$> parseField x

instance FromField (Tagged Bool Data) where
  parseField x =
    Tagged . BoolData <$> do
      case x of
        "true" -> pure True
        "false" -> pure False
        _ -> fail "boolean not parsed"


unt :: Tagged (s :: Type) a -> a
unt = unTagged