{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Influx.Applications.ApplicationsRunning where

import Control.Lens (Ixed (ix), (.~))
import Data.Time (NominalDiffTime)
import Influx.Application (Application (Application), Params)
import Influx.Lib (Bucket, Container (Container), Host (Host))
import Influx.Notification.Targeted (Targeted (Targeted), Targets)
import Influx.Selection (select_bucket, select_from, select_tags)
import Protolude

data ApplicationsRunning

type instance Params ApplicationsRunning = (Container, Host, Bucket, NominalDiffTime, Targets)

applicationsRunning :: Application ApplicationsRunning
applicationsRunning = Application
  do "applications running check"
  do
    \selection (Container name, Host host, bucket', window, targets) ->
      Targeted targets $
        selection
          & select_bucket .~ bucket'
          & select_tags . ix "container_name" .~ name
          & select_tags . ix "host" .~ host
          & select_from .~ window

-- test :: IO ()
-- test =
--   deadmanCheck
--     (const $ pure ()) -- print
--     (Reporter $ const $ pure ())
--     "/secrets/alarms/influx/read.yaml"
--     "../containers/applications-running/data.csv"
--     "../containers/applications-running/template.yaml"
--     "/secrets/alarms/influx/notification.yaml"
--     applicationsRunning
