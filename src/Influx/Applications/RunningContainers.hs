{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Influx.Applications.RunningContainers where

import Control.Lens (Ixed (ix), (.~))
import Influx.Application
import Influx.Lib (Bucket, Container (Container), Host (..))
import Influx.Notification.Targeted
import Influx.Selection (select_bucket, select_tags)
import Protolude ((&))

data RunningContainer

type instance Params RunningContainer = (Container, Host, Bucket, Targets)

runningContainers :: Application RunningContainer
runningContainers = Application
  do "running containers check"
  do
    \selection (Container name, Host host, bucket, targets') ->
      Targeted targets' $
        selection
          & select_bucket .~ bucket
          & select_tags . ix "container_name" .~ name
          & select_tags . ix "host" .~ host
