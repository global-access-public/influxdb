{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Notification.Telegram.Access where

import Data.Yaml (decodeFileThrow)
import Network.HTTP.Client (Manager)
import Network.HTTP.Client.TLS (newTlsManager)
import Protolude
import Servant.API
import Servant.Client
  ( BaseUrl (BaseUrl)
  , ClientEnv
  , ClientM
  , HasClient (Client)
  , Scheme (Https)
  , client
  , mkClientEnv
  , runClientM
  )
import Deriving.Aeson
import Data.Yaml.Aeson (Value)
import Influx.Lib (Logger)
import Control.Monad.Catch (catchAll, MonadCatch)

newtype Token = Token Text
  deriving newtype (FromJSON, ToJSON, Show)

-- | prefix type for all authenticated points on telegramDB API
type AuthorizedRoot r = Capture "auth" Token :> r

newtype Telegram a = Telegram (ReaderT Config ClientM a)
  deriving newtype
    (Functor, Applicative, Monad, MonadIO, MonadReader Config)


data Config = Config
  { config_token :: Token
  , config_chat :: Int
  }
  deriving (Generic)
  deriving
    (FromJSON)
    via CustomJSON '[FieldLabelModifier '[StripPrefix "config_"]] Config

data CallTelegram = CallTelegram (forall m . (MonadIO m , MonadCatch m) => Telegram () -> m ())



data TelegramLog
  = TelegramFailed SomeException
  | TelegramSent Text
  deriving (Show)

-- | introduce a configuration and a manager
bootTelegram :: (MonadIO m, MonadCatch m) => Logger TelegramLog -> FilePath -> m CallTelegram
bootTelegram logger confFile = do
  mc <- decodeFileThrow confFile
  case mc of
    Nothing -> panic "no configuration parse"
    Just c -> catchAll
      do
        manager <- liftIO newTlsManager
        pure $ CallTelegram $ \command -> catchAll
          do request c manager command
          do logger . TelegramFailed
      do \e -> do
          logger $ TelegramFailed e
          pure $ CallTelegram $ const $ pure ()

-- make a servan environment
env :: Manager -> ClientEnv
env manager = mkClientEnv manager (BaseUrl Https "api.telegram.org" 443 "")

request :: MonadIO m => Config -> Manager -> Telegram b -> m b
request config manager (Telegram f) = do
  result <- liftIO $ runClientM
    do runReaderT f config
    do env manager
  case result of
    Left e -> throwIO e
    Right x -> pure x

telegramClient
  :: ( HasClient ClientM api
     , Client ClientM api ~ (Token -> t)
     )
  => Proxy api
  -> (t -> ClientM a)
  -> Telegram a
telegramClient p f =
  Telegram $
    ask >>= \config ->
      lift $ f $ client p $ config_token config

data Message = Message
  {
    message_chat_id                  :: Int -- ^ Unique identifier for the target chat or username of the target channel (in the format @@channelusername@)
  , message_text                     :: Text -- ^ Text of the message to be sent
  }
  deriving (Generic)
  deriving
    (ToJSON)
    via CustomJSON '[FieldLabelModifier '[StripPrefix "message_"]] Message

type MessageAPI = AuthorizedRoot ("sendMessage" :> ReqBody '[JSON] Message :> Post '[JSON] Value)

send :: Text -> Telegram Value
send message = do
  chat <- asks config_chat
  telegramClient (Proxy @MessageAPI) \f -> f $ Message chat message

instance ToHttpApiData Token where
  toUrlPiece (Token t) = "bot" <> t

test :: IO ()
test = do
  CallTelegram f <- bootTelegram print "/secrets/telegram.yaml"
  f $ send "hello" >>= print
