{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Notification.Email where

import Control.Lens (lazy, (^.))
import Control.Monad.Catch (catchAll)
import Data.Aeson (FromJSON)
import Data.Yaml (decodeFileThrow)
import Deriving.Aeson
  ( CamelToSnake
  , CustomJSON (CustomJSON)
  , FieldLabelModifier
  )
import Influx.Lib (Logger)
import Network.Mail.Mime (plainPart)
import Network.Mail.SMTP
  ( Address (Address)
  , sendMail'
  , simpleMail
  )
import Protolude
import Influx.Notification.Targeted

data EmailConf = EmailConf
  { emailHost :: Text
  , emailPort :: Int
  , emailSender :: Text
  , emailDomain :: Text
  }
  deriving stock (Generic, Show)
  deriving
    (FromJSON)
    via CustomJSON '[FieldLabelModifier '[CamelToSnake]] EmailConf

data Email = Email
  { emailTarget :: [Target]
  , emailTitle :: Text
  , emailBody :: Text
  }
  deriving (Show)

notify :: EmailConf -> Email -> IO ()
notify EmailConf {..} Email {..} = sendMail' (toS emailHost) (fromIntegral emailPort) $
  simpleMail
    do Address Nothing emailSender
    do Address Nothing . (<> "@" <> emailDomain) . targetText <$> emailTarget
    do []
    do []
    do emailTitle
    do [plainPart $ emailBody ^. lazy]

data EmailLog
  = EmailFailed SomeException
  | EmailSent [Target] Text
  deriving (Show)

bootEmailSender
  :: Logger EmailLog -- ^ logging
  -> FilePath -- ^ configuration file
  -> IO (Email -> IO ())
bootEmailSender logger file = do
  conf <- decodeFileThrow file
  pure $ \email@Email {..} -> catchAll
    do
      notify conf email
      logger $ EmailSent emailTarget emailTitle
    do logger . EmailFailed

