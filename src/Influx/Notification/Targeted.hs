{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE StandaloneDeriving #-}

module Influx.Notification.Targeted where

import Data.Csv ( FromField(..) )
import Protolude
import qualified Data.Text as T

newtype Target = Target {targetText :: Text} deriving (Show, Eq, Ord)

newtype Targets = Targets {targets ::  [Target]} deriving (Show, Eq, Ord)

instance FromField Targets where 
  parseField = pure . Targets . fmap 
        do Target . T.strip 
        . T.words 
        . decodeUtf8 

data Targeted a = Targeted
  { -- |
    targetedTarget :: Targets
  , -- |
    targetedValue :: a
  }
  deriving (Show, Functor)
