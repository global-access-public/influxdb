{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Influx.Read where

import Control.Lens hiding ((.=), (:>))
import Data.Aeson (FromJSON, KeyValue ((.=)), ToJSON, Value, object)
import Influx.Access (AuthorizedRoot)
import Influx.AccessStreaming (InfluxS, StreamP, influxClientS)
import Influx.Lib (Logger, ReqParam, Organization (Organization))
import Protolude hiding (FilePath)
import Servant.API
  ( Accept (contentType)
  , JSON
  , MimeUnrender (..)
  , NoFraming
  , ReqBody
  , StreamPost
  , type (:>)
  )
import Servant.CSV.Cassava (CSV)

data ByteStringLine

instance Accept ByteStringLine where
  contentType _ = contentType (Proxy @CSV)

instance MimeUnrender ByteStringLine ByteString where
  mimeUnrender _ = Right . view strict

type QueryS =
  "query"
    :> ReqParam "org" Organization
    :> ReqBody '[JSON] Value
    :> StreamPost NoFraming ByteStringLine (StreamP ByteString)

newtype ReadConfig = ReadConfig
  { readOrg :: Organization
  }
  deriving stock (Generic)
  deriving newtype (FromJSON, ToJSON, Show)

newtype QueryLog = QueryLog Text deriving (Show)

-- | query to get a stream of ByteString
queryS
  :: Logger QueryLog
  -> Text
  -> InfluxS ReadConfig (StreamP ByteString)
queryS logger request = influxClientS (Proxy @(AuthorizedRoot QueryS)) $
  \ReadConfig {..} client -> do
    logger $ QueryLog request
    client readOrg $
      object
        [ "query" .= request
        ]
