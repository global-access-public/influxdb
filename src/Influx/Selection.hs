{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

module Influx.Selection where

import Control.Lens (makeLenses)
import qualified Data.Map as M
import qualified Data.Text as T
import Data.Time (NominalDiffTime, UTCTime, addUTCTime)
import Data.Time.Format.ISO8601 (iso8601Show)
import Data.Yaml (FromJSON (parseJSON), withText)
import Deriving.Aeson
  ( CustomJSON (CustomJSON)
  , FieldLabelModifier
  , StripPrefix
  )
import Influx.Data (E (..), unt)
import Influx.Lib (Bucket (..))
import Influx.Line
  ( Measurement (Measurement)
  , Tag (Tag)
  , Tags
  , fromTags
  )
import NeatInterpolation (text)
import Protolude

data DataType
  = DataInt
  | DataDouble
  | DataBool
  | DataText
  deriving (Show, Generic, Hashable, Eq)

instance FromJSON DataType where
  parseJSON = withText "DataType" $ \case
    "int" -> pure DataInt
    "bool" -> pure DataBool
    "double" -> pure DataDouble
    "string" -> pure DataText
    _ -> mzero

data Selection = Selection
  { _select_bucket :: Bucket
  , _select_measurement :: Measurement
  , _select_tags :: Tags
  , _select_from :: NominalDiffTime
  , _select_type :: DataType
  , _select_field :: Text
  , _select_filter :: Maybe Text
  }
  deriving (Generic, Show, Eq)
  deriving
    (FromJSON)
    via CustomJSON '[FieldLabelModifier '[StripPrefix "_"]] Selection

makeLenses ''Selection

instance Hashable Selection where
  hashWithSalt n Selection {..} =
    hashWithSalt
      n
      ( _select_bucket
      , _select_measurement
      , M.assocs _select_tags
      , _select_type
      , _select_field
      , _select_filter
      )

query :: UTCTime -> Selection -> Text
query
  now
  (Selection (Bucket  x) (Measurement m) tags' start _ field regex) =
    [text|
    from(bucket: "$x")
        |> range(start: $startTime, stop: now ())
        |> filter(fn: (r) => r["_measurement"] == "$m")
        $tagsr
        |> filter(fn: (r) => r["_field"] == "$field")
        $filterr
        |> last ()
      |]
    where
      startTime = toS $ iso8601Show $ addUTCTime (negate start) now
      tagsr = case length tags' of
        0 -> ""
        _ -> [text| |> filter(fn: (r) => $cond)|]
      cond = T.intercalate " and " do
        Tag k v <- fromTags tags'
        pure [text|r["$k"] == "$v"|]
      filterr = maybe
        do ""
        do \r -> [text| |> filter(fn: (r) => r["_value"] =~ /$r/)|]
        do regex

selectionDecodeData :: Selection -> E
selectionDecodeData Selection {..} = case _select_type of
  DataInt -> E (unt @Int)
  DataDouble -> E (unt @Double)
  DataText -> E (unt @Text)
  DataBool -> E (unt @Bool)
