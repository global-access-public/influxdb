{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Access where

import Control.Lens (Profunctor (lmap, rmap), makeLensesFor)
import Data.String (String)
import Data.Yaml (FromJSON, ToJSON, decodeFileThrow)
import Network.HTTP.Client (Manager)
import Network.HTTP.Client.TLS (newTlsManager)
import Protolude
import Servant.API
  ( Header'
  , Required
  , type (:>)
  )
import Servant.Client
  ( BaseUrl (BaseUrl)
  , ClientEnv
  , ClientM
  , HasClient (Client)
  , Scheme (Https)
  , client
  , mkClientEnv
  , runClientM
  )
import Influx.Lib (Token)



-- | prefix type for all authenticated points on influxDB API
type AuthorizedRoot r = Header' '[Required] "Authorization" Token :> "api" :> "v2" :> r



-- | connection configuration
data ConnectionConfig = ConnectionConfig
  { token :: Token
  , url :: String -- ^ just the uri
  }
  deriving (Generic, FromJSON, ToJSON, Show)

-- | application configuration
data Config w = Config
  { connection_config :: ConnectionConfig
  , application_config :: w
  }
  deriving (Generic, FromJSON, ToJSON, Functor, Show)

-- internal configuration
data RunConfig w = RunConfig
  { run_token :: Token
  , run_config :: w
  }
  deriving (Generic, Functor)

makeLensesFor
  [ ("run_config", "runConfig")
  ]
  ''RunConfig
newtype Influx w a = Influx (ReaderT (RunConfig w) ClientM a)
  deriving newtype
    (Functor, Applicative, Monad, MonadIO)



instance Profunctor Influx where
  rmap f (Influx r) = Influx $ fmap f r
  lmap f (Influx (ReaderT r)) = Influx $ ReaderT $ \x -> r $ f <$> x

-- | introduce a configuration and a manager
bootInflux :: (MonadIO m, FromJSON w, Show w) => FilePath -> (Config w -> Manager -> a) -> m a
bootInflux confFile cont = do
  mc <- decodeFileThrow confFile
  case mc of
    Nothing -> panic "no configuration parse"
    Just c -> do
      manager <- liftIO newTlsManager
      pure $ cont c manager

-- run in any m a reader with the config
runner :: Config w -> ReaderT (RunConfig w) m a -> m a
runner Config {..} f = runReaderT f $ RunConfig (token connection_config) application_config

-- make a servant environment
env :: Config w -> Manager -> ClientEnv
env Config {..} manager = mkClientEnv manager (BaseUrl Https (url connection_config) 443 "")

nonStreaming :: MonadIO m => Config w -> Manager -> Influx w b -> m b
nonStreaming conf manager (Influx f) = do
  result <- liftIO $ runClientM
    do runner conf f
    do env conf manager
  case result of
    Left e -> throwIO e
    Right x -> pure x

influxClient
  :: ( HasClient ClientM api
     , Client ClientM api ~ (Token -> t)
     )
  => Proxy api
  -> (w -> t -> ClientM a)
  -> Influx w a
influxClient p f =
  Influx $
    ask >>= \(RunConfig token w) ->
      lift $ f w $ client p token
