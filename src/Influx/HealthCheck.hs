{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.HealthCheck where

import Data.Aeson (Value)
import Influx.Access (AuthorizedRoot, Influx, influxClient)
import Protolude
import Servant.API ( JSON, type (:>), Get )

type HealthCheck = AuthorizedRoot ("health" :> Get '[JSON] Value)

healthCheck :: Influx () Value
healthCheck = influxClient
  do Proxy @HealthCheck
  do const identity
