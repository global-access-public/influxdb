{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module Influx.Alarm.Model where

import Data.Time
import Data.Time.Lens ( modL, minutes ) 
import Protolude
import Streaming ( Stream, Of )
import qualified Control.Foldl as L
import qualified Streaming.Prelude as S
import Control.Lens ( Profunctor(lmap) )
import qualified Data.Map.Monoidal.Strict as Mm

data Level = Green | Yellow | Red deriving (Show, Ord, Eq, Enum, Generic)

-- | condition state as in the store
data StoredAlarm = StoredAlarm
  { -- | content identification
    alarm_hash :: Text
  , -- | computed level
    alarm_level :: Level
  , -- | number of sent alerts
    alarm_replica :: Int
  , -- | last alert time
    alarm_time :: UTCTime
  }
  deriving (Generic, Show, Eq)

-- | computed condition
data ConditionAlarm a = ConditionAlarm
  { -- | content identification
    condition_hash :: Text
  , -- | computed level
    condition_level :: Level
  , -- | alerting load
    condition_load :: a
  }
  deriving (Generic, Show, Eq, Functor)


findOrLast :: (Eq t, Num t) => t -> NonEmpty a -> a
findOrLast _ (a :| []) = a
findOrLast 0 (a :| _) = a
findOrLast n (_ :| (r:rs)) = findOrLast (n - 1) (r :| rs)

-- | next replica time
nextReplica
  :: NonEmpty Int
  -> Int -- ^ replica count
  -> UTCTime -- ^ last replica time
  -> UTCTime
nextReplica ks n = modL minutes ( + findOrLast n ks)

-- | an alarm state from a condition
makeStored
  :: Int -- ^ replica count
  -> UTCTime -- ^ new replica time
  -> (ConditionAlarm a) -- ^ condition for the alarm
  -> StoredAlarm
makeStored replica times ConditionAlarm {..} = StoredAlarm
  do condition_hash
  do condition_level
  do replica
  do times

countColors
  :: Monad m => Stream (Of (ConditionAlarm a)) m r
  ->  m (Of (Map Level Int) r)
countColors = L.purely S.fold 
  do Mm.getMonoidalMap . fmap getSum  <$> lmap 
      do \(ConditionAlarm _ l _) ->  Mm.singleton l (Sum 1)
      do L.mconcat