{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Alarm.Configuration where

import Protolude (Applicative ((<*>)), Show, (<$>), Text, FilePath)
import Turtle
  ( Parser
  , optPath, optText
  )

data Configuration = Configuration
  { configEmail :: FilePath
  , configInfluxIn :: FilePath
  , configInfluxOut :: FilePath
  , configQueries :: FilePath
  , configTemplate :: FilePath
  , configOptions :: FilePath
  , configBackoff :: FilePath
  , configGit :: FilePath
  , configFrequency :: Text
  }
  deriving (Show)

configP :: Parser Configuration
configP =
  Configuration
    <$> optPath "email" 'e' "email smtp notification configuration"
    <*> optPath "influx-in" 'i' "influx read instance configuration"
    <*> optPath "influx-out" 'o' "influx write instance configuration"
    <*> optPath "queries" 's' "query parameters to run"
    <*> optPath "template" 't' "query template for parameters"
    <*> optPath "dynamic" 'd' "dynamic options file"
    <*> optPath "backoff" 'b' "path to persitent alarm state"
    <*> optPath "git" 'g' "git url and path to dynamic configs"
    <*> optText "frequency" 'f' "scheduling as in crontab"

