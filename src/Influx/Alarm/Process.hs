{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Alarm.Process where

import Data.Time
import Influx.Alarm.Model
import Protolude
import Streaming
import qualified Streaming.Prelude as S

processAlarms
  :: Monad m
  => NonEmpty Int
  -> UTCTime
  -> (Text -> m (Maybe StoredAlarm)) -- ^ read one
  -> (StoredAlarm -> m ()) -- ^ write / update one
  -> (Text -> m ()) -- ^ delete
  -> Stream (Of (ConditionAlarm a)) m r -- ^ incoming conditions
  -> Stream (Of (ConditionAlarm a)) m r
processAlarms base now readOne write delete proposed = do
  S.for
    do proposed
    do
      \new@ConditionAlarm {..} -> do
        let notGreen = condition_level > Green
        mr <- lift $ readOne condition_hash
        case mr of
          -- condition is not stored
          Nothing -> when
            notGreen
            do
              lift $ write $ makeStored 0 now new
              S.yield new
          Just (StoredAlarm id level rep tim) -> do
            if level /= condition_level
              then do
                if notGreen
                  then lift $ write $ makeStored 0 now new
                  else lift $ delete id
                S.yield new -- send recover status
              else do
                -- equal level
                when
                  notGreen
                  if nextReplica base rep tim <= now
                    then do
                      lift $ write $ makeStored (succ rep) now new
                      S.yield new
                    else pure ()
