{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Influx.Alarm.Email where

import qualified Control.Foldl as L
import qualified Data.Map.Monoidal.Strict as Mm
import qualified Data.Map.Strict as M
import Data.Yaml (ToJSON, encode)
import Influx.Alarm.Model (ConditionAlarm (ConditionAlarm), Level)
import Influx.Notification.Email (Email (Email))
import Influx.Notification.Targeted ( Target, Targeted (Targeted), Targets (Targets))
import NeatInterpolation (text)
import Protolude
import Streaming (Of ((:>)), Stream)
import qualified Streaming.Prelude as S
import Text.Pretty.Simple (pShow)
import Control.Logging (debug')

newtype Alerts a = Alerts {alertsMap :: Map Target (Map Level [a])}

collectEmail
  :: Monad m
  => Stream (Of (ConditionAlarm (Targeted a))) m r
  -> m (Of (Alerts a) r)
collectEmail =
  L.purely S.fold monoidMap . S.map
    do \c@(ConditionAlarm _ _ (Targeted t x)) -> (t, [x <$ c])

monoidMap :: L.Fold (Targets, [ConditionAlarm a]) (Alerts a)
monoidMap = L.Fold
  do
    \m (Targets ts, cs) ->
      m <> fold do
        ConditionAlarm _ l a <- cs
        k <- ts
        pure $ Mm.singleton k (Mm.singleton l [a])
  do mempty
  do Alerts . Mm.getMonoidalMap . fmap Mm.getMonoidalMap

sender :: (ToJSON a, MonadIO m, Show a) => (Email -> m b) -> Text -> Of (Alerts a) r -> m r
sender send applicationName (Alerts results :> r) = do
  debug' $ toS $ pShow results
  forM_ (M.assocs results) $
    \(target, levelSelections) -> forM_ (M.assocs levelSelections) $ \(level, selections) ->
      send $
        Email
          [target]
          do
            let levelS = show level
                count :: Text = show $ length selections
             in [text|[$levelS: $count] $applicationName|]
          do
            decodeUtf8 $
              encode selections
  pure r

sendEmails
  :: (MonadIO m, ToJSON a, Show a)
  => (Email -> m b)
  -> Text
  -> Stream (Of (ConditionAlarm (Targeted a))) m r
  -> m r
sendEmails send title conditions = collectEmail conditions >>= sender send title
