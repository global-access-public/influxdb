{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Influx.Alarm.Deadman.Collect where

import Data.Time.Clock.POSIX ()
import Influx.AccessStreaming (withInfluxS)
import Influx.Alarm.Deadman.LastTime
import Influx.Alarm.Deadman.Run
import Influx.Alarm.Model
import Influx.Lib (Logger)
import Influx.Selection (Selection (..), select_tags)
import Pipes.Safe (MonadCatch, catchAll)
import Protolude
import Streaming (Of (..), Stream)
import qualified Streaming.Prelude as S
import Data.Yaml
import Deriving.Aeson
import Control.Lens
import Influx.Notification.Targeted

-- | evaluate Alarms
evaluateAlarms
  :: (MonadCatch m, MonadIO m)
  => Logger DeadmanRunningLog -- ^ logger
  -> FilePath -- ^ influx config
  -> Stream (Of (Targeted Selection)) m r -- ^ target definitions
  -> Stream (Of (ConditionAlarm (Targeted Report))) m (Of Int r)
evaluateAlarms logger influxConfig selections = do
  w <- liftIO $ withInfluxS influxConfig
  S.map makeAlarm $
    S.map (fmap $ head . catMaybes) $
      S.length $
        S.copy $
          S.for
            do selections
            do
              \t@(Targeted _ s) -> do
                r <- liftIO $ catchAll
                  do Just <$> liftIO (deadmanRun logger w s)
                  do \e -> Nothing <$ (logger . DeadmanRunningQueryFailed) e
                case r of
                  Just x -> S.yield (t, x)
                  Nothing -> pure ()

makeAlarm :: (Targeted Selection, Maybe LastTime) -> ConditionAlarm (Targeted Report)
makeAlarm (t, Nothing) = ConditionAlarm (show $ hash $ targetedValue t) Red $ makeReport <$> t
makeAlarm (t, _) = ConditionAlarm (show $ hash $ targetedValue t) Green $ makeReport <$> t

data Report = Report
  { report_host :: Text
  , report_container :: Text
  }
  deriving (Generic, Show)
  deriving
    (ToJSON)
    via CustomJSON '[FieldLabelModifier '[StripPrefix "report_"]] Report

makeReport :: Selection -> Report
makeReport s = Report
  do s ^. select_tags . ix "host"
  do s ^. select_tags . ix "container_name"
