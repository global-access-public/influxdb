{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Alarm.Deadman.LastTime where

import Data.Csv (FromField, FromNamedRecord (..), (.:))
import Data.Tagged (Tagged (Tagged))
import Data.Time (UTCTime)
import Influx.Data (Data, unt)
import Influx.Lib ()
import Influx.Line (Measurement)
import Protolude

data LastTime = LastTime
  { lastTimeMeasurement :: Measurement
  , lastTimeTime :: UTCTime
  , lastTimeField :: Text
  , lastTimeValue :: Data
  }
  deriving (Show)

instance FromField (Tagged v Data) => FromNamedRecord (Tagged v LastTime) where
  parseNamedRecord v =
    Tagged <$> do
      LastTime <$> v .: "_measurement"
        <*> v .: "_time"
        <*> v .: "_field"
        <*> (unt @v <$> v .: "_value")

