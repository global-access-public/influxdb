{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Influx.Alarm.Deadman.Application where

import Control.Concurrent.SingleJob (singleJob)
import Control.Logging
  ( LogLevel (LevelDebug, LevelInfo, LevelWarn)
  , setLogLevel
  , setLogTimeFormat
  , withStdoutLogging
  )
import Data.Csv (FromRecord)
import Influx.Alarm.Configuration
  ( Configuration
      ( Configuration
      , configBackoff
      , configEmail
      , configFrequency
      , configInfluxIn
      , configInfluxOut
      , configQueries
      , configTemplate, configOptions
      )
  , configGit
  , configP
  )
import Influx.Alarm.Deadman.Deadman (deadmanCheck)
import Influx.Alarm.Deadman.Logger
  ( DeadmanLog
      ( DeadmanApplicationRunOK
      , DeadmanMetrics
      , DeadmanScheduling
      , DeadmanSchedulingStarted
      , DeadmanUpdate
      )
  , topLogger
  )
import Influx.Application (Application, Params)
import Influx.Write (reportInflux)
import Protolude
import System.Cron (MonadSchedule (addJob), execSchedule)
import Turtle (options)
import UpdateConfig.Git
import Data.Yaml (decodeFileThrow, Value (String), FromJSON (parseJSON))
import Deriving.Aeson

instance FromJSON LogLevel where
  parseJSON (String v) = case v of
    "debug" -> pure LevelDebug
    "info" -> pure LevelInfo
    "warn" -> pure LevelWarn
    _ -> mzero
  parseJSON _ = mzero
newtype DynamicOptions = DynamicOptions
  { dynamic_level :: LogLevel
  }
  deriving (Generic, Show)
  deriving
    (FromJSON)
    via CustomJSON '[FieldLabelModifier '[StripPrefix "dynamic_"]] DynamicOptions

program :: (FromRecord (Params a), Show (Params a)) => Application a -> LogLevel -> IO ()
program application level = withStdoutLogging do
  setLogTimeFormat ""
  setLogLevel level
  config@Configuration {..} <- options "containers running check" configP
  reporter <- reportInflux (topLogger . DeadmanMetrics) configInfluxOut
  job <-
    singleJob (topLogger . DeadmanScheduling) $ do
      updateConfig (topLogger . DeadmanUpdate) configGit configQueries configTemplate configOptions
      DynamicOptions {..} <- decodeFileThrow configOptions
      setLogLevel dynamic_level
      deadmanCheck topLogger reporter config application
      topLogger DeadmanApplicationRunOK
  _tids <- execSchedule $ do
    addJob job configFrequency
  topLogger DeadmanSchedulingStarted
  void $ forever $ threadDelay 10_000_000
