{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wall #-}

module Influx.Alarm.Deadman.Deadman where

import Data.Csv (FromRecord)
import qualified Data.Map.Strict as M
import Data.Time (getCurrentTime)
import Data.Time.Clock.POSIX ()
import Data.Yaml (decodeFileThrow)
import Influx.Alarm.Configuration (Configuration (..))
import Influx.Alarm.Deadman.Collect (evaluateAlarms)
import Influx.Alarm.Deadman.Logger (DeadmanLog (..))
import Influx.Alarm.Email (sendEmails)
import Influx.Alarm.Model (ConditionAlarm (condition_level), Level (Green, Red, Yellow), countColors)
import Influx.Alarm.Sqlite (runUpdate)
import Influx.Application (Application (Application), Params)
import Influx.Data
import Influx.Lib (Logger)
import Influx.Line
import Influx.Notification.Email (bootEmailSender)
import Influx.Selection (Selection (..))
import Influx.Write (Reporter (Reporter), Yield (OneLine))
import Pipes.Safe (catchAll, runSafeT)
import Protolude
    ( ($),
      fromIntegral,
      Eq((/=)),
      Monad,
      Show,
      Applicative(pure),
      Int,
      Int64,
      Maybe(Nothing),
      IO,
      Either(..),
      (.),
      Text,
      Map )
import Streaming
import Streaming.Cassava
  ( CsvParseException
  , HasHeader (NoHeader)
  , decodeWithErrors
  , defaultDecodeOptions
  )
import qualified Streaming.Prelude as S
import Streaming.With (withBinaryFileContents)

-- import Control.Logging (debug')

deadmanCheck
  :: forall a.
  (FromRecord (Params a), Show (Params a))
  => Logger DeadmanLog -- ^ logger
  -> Reporter -- ^  metrics
  -> Configuration
  -> Application a -- ^ application template
  -> IO ()
deadmanCheck
  logger
  (Reporter report)
  Configuration {..}
  (Application name row) = runSafeT do
    logger DeadmanStart
    catchAll
      do
        selection :: Selection <- decodeFileThrow configTemplate
        send <- liftIO $ bootEmailSender (logger . DeadmanNotification) configEmail
        now <- liftIO getCurrentTime
        r <-
          withBinaryFileContents configQueries $
            sendEmails (liftIO . send) name
              . S.store S.length
              . runUpdate configBackoff [5, 15, 45, 135] now
              . S.store countColors
              . S.store
                do
                  S.mapM_ (logger . DeadmanReport)
                    . S.filter ((/=) Green . condition_level)
              . evaluateAlarms (logger . DeadmanRunningLog) configInfluxIn
              . S.map (row selection)
              -- . S.chain do debug' . show
              . S.store S.length
              . logErrors logger -- S.concat
              . S.store S.length -- length of params
              . decodeWithErrors defaultDecodeOptions NoHeader
        case r of
          (sent :> colors :> evaluated :> decoded :> required :> pr) -> do
            results
              logger
              report
              name
              colors
              (fromIntegral sent)
              (fromIntegral evaluated)
              (fromIntegral decoded)
              (fromIntegral required)
            case pr of
              Left (e, _) -> logger $ DeadmanParseFailed e
              _ -> pure ()
      do logger . DeadmanEndBad
    logger DeadmanEndGood

results
  :: MonadIO m
  => Logger DeadmanLog -- ^
  -> (Yield -> m ()) -- ^
  -> Text -- ^ application name
  -> Map Level Int -- ^ number of sent alerts
  -> Int64 -- ^ number of sent alerts
  -> Int64 -- ^ number of required
  -> Int64 -- ^ number of decoded checks
  -> Int64 -- ^ number of required checks
  -> m ()
results logger report name colors sent evaluated decoded required = do
  report $
    OneLine $
      Line
        "deadman_metrics"
        [Tag "name" name]
        [ Field "evaluated" $ IntData evaluated
        , Field "sent" $ IntData sent
        , Field "decoded" $ IntData decoded
        , Field "required" $ IntData required
        , Field "red" $ IntData $ fromIntegral $ M.findWithDefault 0 Red colors
        , Field "green" $ IntData $ fromIntegral $ M.findWithDefault 0 Green colors
        , Field "yellow" $ IntData $ fromIntegral $ M.findWithDefault 0 Yellow colors
        ]
        Nothing
  logger (DeadmanRunResult sent evaluated decoded required)
  logger (DeadmanRunColors colors)

logErrors
  :: forall a m r.
  Monad m
  => (DeadmanLog -> m ())
  -> S.Stream (Of (Either CsvParseException a)) m r
  -> S.Stream (Of a) m r
logErrors logger s = S.for s do
  \case
    Left x -> lift $ logger (DeadmanParseFailed x)
    Right x -> S.yield x
