{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Alarm.Deadman.Logger where

import Control.Concurrent.SingleJob (SingleJobLog (..))
import Control.Logging (debug', errorL', log', logS', warn')
import qualified Data.Text as T
import Influx.Alarm.Deadman.Collect (Report)
import Influx.Alarm.Deadman.Run (DeadmanRunningLog (..))
import Influx.Alarm.Model (ConditionAlarm, Level)
import Influx.Notification.Email (EmailLog (..))
import Influx.Notification.Targeted
  ( Target (targetText)
  , Targeted
  )
import Influx.Read (QueryLog (QueryLog))
import Influx.Write (InfluxWriteLog (..))
import NeatInterpolation (text)
import Protolude
import Streaming.Cassava (CsvParseException)
import Text.Pretty.Simple (pShow)
import UpdateConfig.Git (UpdateConfigLog (..))

data DeadmanLog
  = DeadmanRunResult Int64 Int64 Int64 Int64
  | DeadmanStart
  | DeadmanReport
      ( ConditionAlarm
          (Influx.Notification.Targeted.Targeted Report)
      )
  | DeadmanRunningLog DeadmanRunningLog
  | DeadmanNotification EmailLog
  | DeadmanEndBad SomeException
  | DeadmanEndGood
  | DeadmanSchedulingStarted
  | DeadmanScheduling SingleJobLog
  | DeadmanMetrics InfluxWriteLog
  | DeadmanDebug Text
  | DeadmanParseFailed CsvParseException
  | DeadmanApplicationRunOK
  | DeadmanRunColors (Map Level Int)
  | DeadmanUpdate UpdateConfigLog
  deriving (Show)

topLogger :: MonadIO m => DeadmanLog -> m ()
topLogger = \case
  DeadmanUpdate r -> case r of
    UpdateConfigDone -> log' [text| configs updated|]
    UpdateConfigFailed (SomeException (show -> e)) -> warn' [text| failed to update configs: $e|]
    UpdateConfigTryFailed (SomeException (show -> e)) -> debug' [text| try branch failed: $e|]
    UpdateStdErr l -> debug' [text| updating stderr: $l |]
    UpdateStdOut l -> debug' [text| updating stdout: $l |]
    UpdateConfigImpossible -> warn' [text| updating configs was impossible|]
  DeadmanStart -> log' "check started"
  DeadmanParseFailed (show -> e) -> warn' [text| parsing targets: $e|]
  DeadmanRunColors m -> log' $ show m
  DeadmanRunResult sent evaluated decoded required -> do
    let (sentS, evaluatedS, decodedS, requiredS) = (show sent, show evaluated, show decoded, show required)
    log'
      [text|
        $sentS update sent over $evaluatedS evaluated over $decodedS decoded over $requiredS required
        |]
  DeadmanRunningLog r -> case r of
    DeadmanQuery (QueryLog q) ->
      debug'
        [text| query:
      $q |]
    DeadmanResult (show -> l) ->
      debug'
        [text| lasttime:
      $l |]
    DeadmanRawResult (show -> l) ->
      debug'
        [text| lasttimeraw:
      $l |]
    DeadmanRunningQueryFailed (show -> e) -> warn' [text| query failed: $e |]
  DeadmanNotification x -> case x of
    EmailFailed (show -> e) -> warn' [text| email sending: $e|]
    EmailSent rs t ->
      let rcs = T.intercalate ", " (targetText <$> rs)
       in do
            log' [text| email "$t" sent to $rcs|]
  DeadmanEndBad (SomeException e) -> errorL' $ show e
  DeadmanEndGood -> log' "check ended"
  DeadmanSchedulingStarted -> log' "scheduling started"
  DeadmanScheduling SingleJobMissed -> warn' "old job still running, skipping new one"
  DeadmanScheduling (SingleJobRun n) -> log' $ "job run: " <> show n
  DeadmanMetrics (InfluxWriteInitFailed e) -> warn' $ "influx reporting failed: " <> show e
  DeadmanMetrics (InfluxWriteSuccess (show -> n)) ->
    log' [text| influx reporting succeded for $n lines|]
  DeadmanMetrics (InfluxWriteFailed (show -> e) (Just (show -> l))) ->
    warn'
      [text| influx reporting failed for $l lines with exceptio $e|]
  DeadmanMetrics (InfluxWriteFailed (show -> e) Nothing) ->
    warn'
      [text| influx reporting failed with exception $e|]
  DeadmanDebug x -> debug' x
  DeadmanApplicationRunOK -> log' "application ran ok"
  DeadmanReport x -> logS' "deadman positive" $ toS $ pShow x
