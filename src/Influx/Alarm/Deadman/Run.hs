{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Alarm.Deadman.Run where

import Control.Arrow ((>>>))
import qualified Control.Foldl as L
import qualified Data.ByteString as B
import Data.Time (getCurrentTime)
import Influx.AccessStreaming
  ( StreamP (getStreaming)
  , WithInfluxS (WithInfluxS)
  )
import Influx.Alarm.Deadman.LastTime (LastTime (lastTimeMeasurement))
import Influx.Data (E (E))
import Influx.Decode (decodeAllNamedS)
import Influx.Lib (Logger)
import Influx.Read (QueryLog, ReadConfig, queryS)
import Influx.Selection (Selection, query, selectionDecodeData)
import Protolude
import qualified Streaming.Prelude as S

data DeadmanRunningLog
  = DeadmanQuery QueryLog
  | DeadmanResult LastTime
  | DeadmanRawResult ByteString
  | DeadmanRunningQueryFailed SomeException
  deriving (Show)

deadmanRun
  :: Logger DeadmanRunningLog
  -> WithInfluxS ReadConfig
  -> Selection
  -> IO [Maybe LastTime]
deadmanRun logger (WithInfluxS streamOf _valueOf) selection =
  case selectionDecodeData selection of
    E untag -> do
      now <- getCurrentTime
      streamOf
        do
          queryS (logger . DeadmanQuery) $ query now selection
        do
          \s -> do
            S.toList_
              . S.mapped do L.purely S.fold L.last
              . S.groupBy ((==) `on` lastTimeMeasurement)
              . S.chain (logger . DeadmanResult)
              . S.map untag
              . decodeAllNamedS
              . S.chain (logger . DeadmanRawResult)
              . S.map (B.init >>> (<> "\n"))
              . getStreaming
              $ s
