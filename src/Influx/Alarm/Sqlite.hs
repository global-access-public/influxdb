{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{-
We have 2 tables.
1) condition configuration

2) alarm state

-}

module Influx.Alarm.Sqlite where

import Data.Time (UTCTime)
import Database.SQLite.Simple
import Database.SQLite.Simple.FromField
import Database.SQLite.Simple.ToField
import Influx.Alarm.Model
import Influx.Alarm.Process
import Influx.Lib ()
import Pipes.Safe
import Protolude
import Streaming (Of, Stream)
import Prelude hiding (head, show)

instance ToField Level where
  toField =
    \case
      Green -> SQLText "green"
      Yellow -> SQLText "yellow"
      Red -> SQLText "red"

instance FromField Level where
  fromField s = do
    r :: Text <- fromField s
    case r of
      "green" -> pure Green
      "yellow" -> pure Yellow
      "red" -> pure Red
      _ -> mzero

instance ToRow StoredAlarm where
  toRow StoredAlarm {..} = toRow (alarm_hash, alarm_level, alarm_replica, alarm_time)

instance FromRow StoredAlarm where
  fromRow = StoredAlarm <$> field <*> field <*> field <*> field

-- | extract stored alarms state
openDatabase :: FilePath -> IO Connection
openDatabase fp = do
  conn <- open fp
  execute_
    conn
    "CREATE TABLE IF NOT EXISTS state \
    \ (hash TEXT unique , level TEXT, replica INT, time TEXT)"
  pure conn

cleanDatabase :: FilePath -> IO ()
cleanDatabase fp = withConnection fp $ \c -> execute_ c "delete from state"

sqliteProcess
  :: MonadIO m
  => Connection
  -> NonEmpty Int
  -> UTCTime
  -> Stream (Of (ConditionAlarm a)) m r
  -> Stream (Of (ConditionAlarm a)) m r
sqliteProcess connection backoffs now = processAlarms
  backoffs
  now
  do \h -> liftIO $ head <$> query connection "SELECT * from state where hash = (?)" (Only h)
  do
    \s@StoredAlarm {..} ->
      liftIO $
        execute
          connection
          "INSERT INTO state VALUES(?,?,?,?) \
          \ ON CONFLICT(hash) DO UPDATE SET level = ?, replica = ?, time = ? ;"
          (s :. (alarm_level, alarm_replica, alarm_time))
  do liftIO . execute connection "DELETE FROM state where hash = ?" . Only

runUpdate
  :: (MonadIO (Base m), MonadSafe m)
  => FilePath
  -> NonEmpty Int
  -> UTCTime
  -> Stream (Of (ConditionAlarm a)) m b
  -> Stream (Of (ConditionAlarm a)) m b
runUpdate fp r t s = do
  c <- liftIO $ openDatabase fp
  void $ lift $ register $ liftIO $ close c
  sqliteProcess c r t s

getDatabase :: FilePath -> IO [StoredAlarm]
getDatabase fp = withConnection fp $ \c -> query_ c "select * from state"


