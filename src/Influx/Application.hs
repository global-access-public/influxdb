{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.Application where

import Influx.Selection (Selection)
import Protolude
import Influx.Notification.Targeted

type family Params a

data Application a = Application
  { applicationName :: Text
  , applicationMakeRow :: Selection -> Params a -> Targeted Selection
  }
