{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Influx.Write where

import Control.Concurrent.STM (modifyTVar, newTVarIO, readTVarIO)
import Control.Lens
import Data.Aeson (FromJSON, ToJSON)
import Influx.Access
  ( AuthorizedRoot
  , Influx
  , influxClient
  )
import Influx.AccessStreaming (StreamP (..), WithInfluxS (WithInfluxS), withInfluxS)
import Influx.Lib (Bucket, Logger, Organization, ReqParam)
import Influx.Line (Line, Precision (MilliSecond, Second), renderLine)
import Pipes.Safe (catchAll)
import Protolude hiding (lines)
import Servant.API
  ( PlainText
  , StreamBody
  , ToHttpApiData (toUrlPiece)
  , type (:>), NewlineFraming, PostNoContent
  )
import Streaming hiding (yields)
import qualified Streaming.Prelude as S

instance ToHttpApiData Precision where
  toUrlPiece Second = "s"
  toUrlPiece MilliSecond = "ms"

type Write =
  "write"
    :> ReqParam "org" Organization
    :> ReqParam "bucket" Bucket
    :> ReqParam "precision" Precision
    :> StreamBody NewlineFraming  PlainText (StreamP Text)
    :> PostNoContent

data WriteConfig = WriteConfig
  { org :: Organization
  , bucket :: Bucket
  , precision :: Precision
  }
  deriving (Generic, FromJSON, ToJSON, Show)

makeLensesFor
  [ ("org", "orgL")
  , ("bucket", "bucketL")
  , ("precision", "precisionL")
  ]
  ''WriteConfig

writeStream :: StreamP Line -> Influx WriteConfig Int
writeStream (StreamP lines) = do
  count <- liftIO $ newTVarIO 0
  void $
    influxClient (Proxy @(AuthorizedRoot Write)) $
      \WriteConfig {..} client ->
        client org bucket precision $
          StreamP $
            S.chain
              do const $ liftIO $ atomically $ modifyTVar count succ
              do S.map (renderLine precision) lines
  liftIO $ readTVarIO count

safeWrite
  :: (StreamP Line -> SomeException -> IO ()) -- failure
  -> WithInfluxS WriteConfig
  -> StreamP Line
  -> IO ()
safeWrite warn (WithInfluxS _ nostream) l = catchAll
  do void $ nostream $ writeStream l
  do warn l

data InfluxWriteLog
  = InfluxWriteInitFailed SomeException
  | InfluxWriteFailed SomeException (Maybe Int)
  | InfluxWriteSuccess Int
  deriving (Show)

safeWriteLogging :: Logger InfluxWriteLog -> WithInfluxS WriteConfig -> StreamP Line -> IO ()
safeWriteLogging logger = safeWrite $ \_ -> logger . InfluxWriteInitFailed

data Yield where
  OneLine :: Line -> Yield
  MultiLine :: [Line] -> Yield
  YieldStream :: Stream (Of Line) IO () -> Yield

-- sizeOfYield

yields :: Yield -> StreamP Line
yields =
  StreamP . \case
    OneLine l -> S.yield l
    MultiLine ls -> S.each ls
    YieldStream ls -> ls

newtype Reporter = Reporter (forall m. MonadIO m => Yield -> m ())

reportInflux :: Logger InfluxWriteLog -> FilePath -> IO Reporter
reportInflux logger c = catchAll
  do withInfluxS c <&> \s -> Reporter $ liftIO . safeWriteLogging logger s . yields
  do
    \e -> do
      logger $ InfluxWriteInitFailed e
      pure $
        Reporter $
          liftIO . S.length_ . getStreaming . yields >=> logger . InfluxWriteFailed e . Just

newtype ReporterS = ReporterS (Yield -> Stream (Of InfluxWriteLog) IO Bool)

reportInfluxInitS :: FilePath -> IO ReporterS
reportInfluxInitS c = do
  withInfluxS c <&> \(WithInfluxS _ w) -> ReporterS $ \y -> do
    r <- liftIO $ catchAll
      do (w . writeStream . yields) y <&> Right
      do pure . Left
    case r of
      Right n -> S.yield (InfluxWriteSuccess n) $> True
      Left se ->
        S.yield (InfluxWriteFailed se Nothing) $> False

reportInfluxS :: FilePath -> Stream (Of InfluxWriteLog) IO ReporterS
reportInfluxS c = do
  r <- liftIO $ catchAll
    do Right <$> reportInfluxInitS c
    do pure . Left
  either
    do
      \e -> do
        S.yield (InfluxWriteInitFailed e)
        pure $
          ReporterS $ \y -> do
            n <- liftIO . S.length_ . getStreaming . yields $ y
            S.yield . InfluxWriteFailed e . Just $ n
            pure False
    do pure
    do r
