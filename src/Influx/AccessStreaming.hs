{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Influx.AccessStreaming where

import Control.Lens (Profunctor (lmap, rmap), over)
import Control.Monad.Base (MonadBase)
import Control.Monad.Catch (MonadMask)
import Control.Monad.Reader (withReaderT)
import Control.Monad.Trans.Control (MonadBaseControl, control)
import Data.Aeson (FromJSON)
import Influx.Access
  ( Config
  , Influx
  , RunConfig (RunConfig, run_config)
  , bootInflux
  , env
  , nonStreaming
  , runConfig
  , runner
  )
import Influx.Lib ( Token )
import Network.HTTP.Client (Manager)
import Protolude
import Servant.API
  ( FromSourceIO (fromSourceIO)
  , ToSourceIO (..)
  )
import Servant.Client.Streaming
  ( ClientM
  , HasClient (Client)
  , client
  , withClientM
  )
import Servant.Types.SourceT (SourceT (unSourceT), StepT (..), fromStepT)
import Streaming ( effect, Stream, Of(..) )
import Streaming.Concurrent (bounded, withBuffer, withStreamBasket, writeStreamBasket)
import qualified Streaming.Internal as S
import qualified Streaming.Prelude as S


-- ToDo, use streaming-nonempty as at least writing fails ?
newtype StreamP a = StreamP {getStreaming :: Stream (Of a) IO ()}
  deriving newtype (Semigroup, Monoid)

instance FromSourceIO a (StreamP a) where
  fromSourceIO src = pure $ StreamP $ effect $ unSourceT src (pure . go)
    where
      go :: StepT IO a -> Stream (Of a) IO ()
      go Stop = S.Return ()
      go (Error err) = effect $ panic $ toS err
      go (Skip s) = go s -- drives
      go (Effect ms) = effect $ fmap go ms
      go (Yield x s) = S.yield x >> go s

instance ToSourceIO a (StreamP a) where
  toSourceIO (StreamP x) = fromStepT $ go x
    where
      go (S.Return _) = Stop
      go (S.Effect f) = Effect $ fmap go f
      go (S.Step (e :> rest)) = Yield e $ go rest

data WithInfluxS w = WithInfluxS
  { streamIn
    :: forall a b.
            InfluxS w (StreamP a)
         -> (StreamP a -> IO b)
         -> IO b
  , runOne :: forall a. Influx w a -> IO a
  }

newtype InfluxS w a = InfluxS (ReaderT (RunConfig w) ClientM a)
  deriving newtype
    (Functor, Applicative, Monad, MonadIO)

instance MonadReader w (InfluxS w) where
  ask = InfluxS $ asks run_config
  local f (InfluxS g) = InfluxS $ local (over runConfig f) g

instance Profunctor InfluxS where
  rmap f (InfluxS r) = InfluxS $ fmap f r
  lmap f (InfluxS r) = InfluxS $ withReaderT (fmap f) r

streaming
  :: Config w
  -> Manager
  -> InfluxS w (StreamP a)
  -> (StreamP a -> IO b)
  -> IO b
streaming conf manager (InfluxS f) g = do
  liftIO $ withClientM
    do runner conf f
    do env conf manager
    do
      \case
        Left e -> panic $ show e
        Right s -> g s

withInfluxS :: (FromJSON w, Show w) => FilePath -> IO (WithInfluxS w)
withInfluxS confFile = bootInflux confFile $ \conf manager -> do
  WithInfluxS (streaming conf manager) (nonStreaming conf manager)

influxClientS
  :: ( HasClient ClientM api
     , Client ClientM api ~ (Token -> t)
     )
  => Proxy api
  -> (w -> t -> ClientM a)
  -> InfluxS w a
influxClientS p f =
  InfluxS $
    ask >>= \(RunConfig token' w) ->
      lift $ f w $ client p token'

instance Functor WithInfluxS where
  fmap :: (w -> w') -> WithInfluxS w -> WithInfluxS w'
  fmap f (WithInfluxS s t) = WithInfluxS
    do s . lmap f
    do t . lmap f

streamingT
  :: MonadBaseControl IO m
  => Config w
  -> Manager
  -> InfluxS w (StreamP a)
  -> (StreamP a -> m b)
  -> m b
streamingT c m iw q = control $ \runInBase -> streaming c m iw (runInBase . q)

baseConsumer
  :: ( MonadMask m
     , MonadBaseControl IO m
     , MonadBase IO n
     )
  => Stream (Of a) m ()
  -> (Stream (Of a) n () -> m r)
  -> m r
baseConsumer s = withBuffer (bounded 1000) (writeStreamBasket s) . flip withStreamBasket
