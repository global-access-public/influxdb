{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

{-
We have 2 tables.
1) condition configuration

2) alarm state

-}

module Test.Alarm.Model where

import Control.Arrow
import Control.Lens hiding (Level, (:>))
import Control.Monad.Free
import Control.Monad.Free.TH
import qualified Data.Map as M
import Data.Time (ParseTime, UTCTime, defaultTimeLocale, getCurrentTime, parseTimeM)
import Influx.Alarm.Model
import Influx.Alarm.Process
import Influx.Alarm.Sqlite (cleanDatabase, getDatabase)
import qualified Influx.Alarm.Sqlite as SQL
import Influx.Lib ()
import Pipes.Safe (runSafeT)
import Protolude
import Streaming (Of, Stream, hoist)
import qualified Streaming.Prelude as S
import System.IO.Unsafe
import Test.Hspec (hspec, describe, shouldBe, it)

type Database = Map Text StoredAlarm

mkDatabase :: [StoredAlarm] -> Database
mkDatabase = M.fromList . fmap (alarm_hash &&& identity)

timeIs :: (MonadFail m, ParseTime t) => String -> m t
timeIs = parseTimeM True defaultTimeLocale "%s"

data Update m a
  = UpdateTest
      UTCTime
      (Stream (Of (ConditionAlarm ())) m ())
      a
  deriving (Functor)

makeFree ''Update

runUpdateMemory :: Run
runUpdateMemory = Run $ go ([], [])
  where
    go x (Pure _) = x
    go (cs, db) (Free (UpdateTest time input f)) =
      let (cs', db') = runState (S.toList_ . updateAlarms [2] time $ input) db
       in go (cs <> cs', db') f

updateAlarms
  :: NonEmpty Int
  -> UTCTime
  -> Stream (Of (ConditionAlarm ())) (State Database) r
  -> Stream (Of (ConditionAlarm ())) (State Database) r
updateAlarms d now = processAlarms
  d
  now
  do \x -> preuse $ ix x
  do \s@StoredAlarm {..} -> modify $ M.insert alarm_hash s
  do modify . M.delete

data Run = forall m. Monad m => Run (Free (Update m) () -> ([ConditionAlarm ()], Database))

-- runUpdateSQL :: Free (Update IO) () -> ([ConditionAlarm ()], Database)
runUpdateSQL :: Run
runUpdateSQL = Run $ \s -> unsafePerformIO do
  void $ cleanDatabase "test.sql"
  go [] s
  where
    go :: [ConditionAlarm ()] -> Free (Update IO) x -> IO ([ConditionAlarm ()], Database)
    go r (Pure _) = (r,) . mkDatabase <$> getDatabase "test.sql"
    go cs (Free (UpdateTest time input f)) = do
      cs' <- runSafeT $ S.toList_ $ SQL.runUpdate "test.sql" [2] time $ hoist lift input
      go (cs <> cs') f

newAlarm' :: Text -> Level -> ConditionAlarm ()
newAlarm' h l = ConditionAlarm h l ()

test :: IO ()
test = hspec do
  forM_ @[] [runUpdateSQL] $ \(Run runUpdate) ->
    describe "one run" do
      it "does nothing for nothing" do
        now <- getCurrentTime
        shouldBe
          do runUpdate $ updateTest now $ pure ()
          do ([], [])
      it " adds a green" do
        now <- timeIs "0"
        let a1 = newAlarm' "a1" Green
        shouldBe
          do runUpdate $ updateTest now do { S.yield a1 }
          do ([], [])
      it " adds a yellow" do
        now <- timeIs "0"
        let a1 = newAlarm' "a1" Yellow
        shouldBe
          do runUpdate $ updateTest now do { S.yield a1 }
          do ([a1], mkDatabase [makeStored 0 now a1])
      it " adds a red" do
        now <- timeIs "0"
        let a1 = newAlarm' "a1" Red
        shouldBe
          do runUpdate $ updateTest now do { S.yield a1 }
          do ([a1], mkDatabase [makeStored 0 now a1])
      it "ignore 2nd yellow" do
        now <- timeIs "0"
        later <- timeIs "1"
        let a1 = newAlarm' "a1" Yellow
        shouldBe
          do
            runUpdate do
              updateTest now do S.yield a1
              updateTest later do S.yield a1
          do ([a1], mkDatabase [makeStored 0 now a1])
      it "ignore 2nd red" do
        now <- timeIs "0"
        later <- timeIs "1"
        let a1 = newAlarm' "a1" Red
        shouldBe
          do
            runUpdate do
              updateTest now do S.yield a1
              updateTest later do S.yield a1
          do ([a1], mkDatabase [makeStored 0 now a1])
      it "ignore 2nd green" do
        now <- timeIs "0"
        later <- timeIs "1"
        let a1 = newAlarm' "a1" Green
        shouldBe
          do
            runUpdate do
              updateTest now do S.yield a1
              updateTest later do S.yield a1
          do ([], [])

      it "pass from green to yellow" do
        now <- timeIs "0"
        later <- timeIs "1"
        let a1 = newAlarm' "a1" Green
            a2 = newAlarm' "a1" Yellow
        shouldBe
          do
            runUpdate do
              updateTest now do S.yield a1
              updateTest later do S.yield a2
          do ([a2], mkDatabase [makeStored 0 later a2])
      it "pass from green to red" do
        now <- timeIs "0"
        later <- timeIs "1"
        let a1 = newAlarm' "a1" Green
            a2 = newAlarm' "a1" Red
        shouldBe
          do
            runUpdate do
              updateTest now do S.yield a1
              updateTest later do S.yield a2
          do ([a2], mkDatabase [makeStored 0 later a2])
      it "pass from yellow to red" do
        now <- timeIs "0"
        later <- timeIs "1"
        let a1 = newAlarm' "a1" Yellow
            a2 = newAlarm' "a1" Red
        shouldBe
          do
            runUpdate do
              updateTest now do S.yield a1
              updateTest later do S.yield a2
          do ([a1, a2], mkDatabase [makeStored 0 later a2])

-- it "pass from yellow to green" do
--   now <- timeIs "0"
--   later <- timeIs "1"
--   let a1 = newAlarm' "a1" Yellow
--       a2 = newAlarm' "a1" Green
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later do S.yield a2
--     do ([a1, a2], [])
-- it "pass from red to green" do
--   now <- timeIs "0"
--   later <- timeIs "1"
--   let a1 = newAlarm' "a1" Red
--       a2 = newAlarm' "a1" Green
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later do S.yield a2
--     do ([a1, a2], [])
-- it "pass from red to yellow" do
--   now <- timeIs "0"
--   later <- timeIs "1"
--   let a1 = newAlarm' "a1" Red
--       a2 = newAlarm' "a1" Yellow
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later do S.yield a2
--     do ([a1, a2], mkDatabase [makeStored 0 later a2])
-- it "doesn't replicate green" do
--   now <- timeIs "0"
--   later <- timeIs "90"
--   let a1 = newAlarm' "a1" Green
--       a2 = newAlarm' "a1" Green
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later do S.yield a2
--     do ([],  [])
-- it "replicate yellow after delta" do
--   now <- timeIs "0"
--   later <- timeIs "120"
--   let a1 = newAlarm' "a1" Yellow
--       a2 = newAlarm' "a1" Yellow
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later  do S.yield a2
--     do ([a1,a2],  mkDatabase [makeStored 1 later a2])
-- it "doesn't replicate yellow after delta - e minutes" do
--   now <- timeIs "0"
--   later <- timeIs "119"
--   let a1 = newAlarm' "a1" Yellow
--       a2 = newAlarm' "a1" Yellow
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later do S.yield a2
--     do ([a1],  mkDatabase [makeStored 0 now a1])
-- it "doesn't replicate 2 yellow after delta + delta ^ 2 " do
--   now <- timeIs "0"
--   later <- timeIs "120"
--   later2 <- timeIs "360"
--   let a1 = newAlarm' "a1" Yellow
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later do S.yield a1
--         updateTest later2 do S.yield a1
--     do ([a1,a1,a1],  mkDatabase [makeStored 2 later2 a1])
-- it "doesn't replicate 2 yellow after delta + delta ^ 2 minutes - 1 sec" do
--   now <- timeIs "0"
--   later <- timeIs "120"
--   later2 <- timeIs "359"
--   let a1 = newAlarm' "a1" Yellow
--   shouldBe
--     do
--       runUpdate do
--         updateTest now do S.yield a1
--         updateTest later do S.yield a1
--         updateTest later2 do S.yield a1
--     do ([a1,a1],  mkDatabase [makeStored 1 later a1])
