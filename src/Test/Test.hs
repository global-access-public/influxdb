{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Test where

import qualified Control.Foldl as L
import Control.Lens (Profunctor (lmap), (.~))
import Data.Csv (FromNamedRecord (parseNamedRecord), (.:))
import Data.Time (UTCTime)
import Data.Time.Clock.POSIX (POSIXTime, getPOSIXTime)
import Influx.AccessStreaming
  ( StreamP (StreamP, getStreaming)
  , WithInfluxS (WithInfluxS)
  , withInfluxS
  )
import Influx.Decode (decodeAllNamedS)
import Influx.Line
  (
   Field (Field)
  , Line (Line)
  , Tag (Tag), Precision (MilliSecond)
  )
import Influx.Read (ReadConfig (ReadConfig), queryS)
import Influx.Write
  (  WriteConfig (org)
  , precisionL
  , writeStream
  )
import NeatInterpolation (text)
import Protolude
import qualified Streaming.Prelude as S
import System.Random.Stateful (randomRM, newIOGenM, mkStdGen)
import Influx.Data ( Data(DoubleData) )

generateLine :: POSIXTime -> S.Stream (S.Of Line) IO b
generateLine t = do
  v <- liftIO $ newIOGenM (mkStdGen 42) >>= randomRM (0, 100)
  S.yield $
    Line
      "cpu_load"
      [Tag "host" "server01", Tag "alley" "east"]
      [ Field "cpu-1" (DoubleData v)
      ]
      $ Just t
  generateLine $ t + 10

test :: IO ()
test =  do
  WithInfluxS streamOf valueOf <- withInfluxS "default.yaml"
  -- lmap (const ()) healthCheck >>= print
  t <- getPOSIXTime
  void $ valueOf do
    lmap (precisionL .~ MilliSecond) $
      writeStream $ StreamP $ S.take (2 * 8640) $ generateLine $ t - 2 * 86400

  result <- streamOf
    do lmap (ReadConfig . org) $ queryS print q1
    do L.purely S.fold_ (lmap cpu L.mean) . decodeAllNamedS @CPULoad . getStreaming
  print result

cpu :: CPULoad -> Double
cpu (CPU1 _ x _) = x
cpu (CPU2 _ x _) = x

data CPULoad
  = CPU1 {time :: UTCTime, v1 :: Double, alley :: Text}
  | CPU2 {time :: UTCTime, v2 :: Double, alley :: Text}
  deriving (Show)

instance FromNamedRecord CPULoad where
  parseNamedRecord v = do
    t <- v .: "_field"
    r <- case t of
      ("cpu-1" :: Text) -> pure CPU1
      ("cpu-2" :: Text) -> pure CPU2
      _ -> mzero
    r <$> v .: "_time" <*> v .: "_value" <*> v .: "alley"

q1 :: Text
q1 =
  [text|
    from(bucket: "test")
        |> range(start: -20m, stop: now ())
        |> filter(fn: (r) => r["_measurement"] == "cpu_load")
        |> filter(fn: (r) => r["host"] == "server01")
        |> keep (columns: ["_time", "_field", "_value", "alley"])
        |> yield (name: "hello")
  |]
